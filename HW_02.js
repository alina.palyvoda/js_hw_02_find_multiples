/**
 * Created by Алина on 15.01.2022.
 */

// Теоретический вопрос
//1. Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
    // Циклы в программировании нужны для того, чтобы повторять определенное дейсвие
    // необходимое количество раз.

let userNumber = +prompt ("Enter the number");
while (Number.isNaN(userNumber) || userNumber < 0 || !Number.isInteger(userNumber)) {
    userNumber = +prompt("Incorrect value! Enter the number again");
}
if (userNumber >= 0 && userNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 === 0) {
           console.log(i);
        }
    }
}




