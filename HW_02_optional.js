let firstNumber = +prompt("Enter the first number");
let secondNumber = +prompt("Enter the second number");
while (Number.isNaN(firstNumber) || firstNumber < 0 || !Number.isInteger(firstNumber) ||
Number.isNaN(secondNumber) || secondNumber < 0 || !Number.isInteger(secondNumber)) {
    firstNumber = +prompt("Incorrect value! Enter the first number again");
    secondNumber = +prompt("Enter the second number again");
}
if (firstNumber < secondNumber) {
    nextPrime: for (let i = firstNumber; i <= secondNumber; i++) {
        for (let j = 2; j < i; j++) {
            if (i % j === 0) continue nextPrime;
        }
        if (i > 1) {
            console.log(i)
        }
    }
} else if (secondNumber < firstNumber) {
    nextPrime: for (let i = secondNumber; i <= firstNumber; i++) {
        for (let j = 2; j < i; j++) {
            if (i % j === 0) continue nextPrime;
        }
        if (i > 1) {
            console.log(i)
        }
    }
}
